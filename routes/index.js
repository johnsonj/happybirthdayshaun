var express = require('express');
var router = express.Router();

function randomShaun()
{
  var seans = ['Sean', 'Shawn', 'Shaun', 'Shone', 'Chone'];
  return seans[Math.floor(Math.random() * seans.length)];
}

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { shaun: randomShaun() });
});


module.exports = router;
